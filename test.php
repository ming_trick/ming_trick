<?php
//Connecting to db
$mysqli = new mysqli("localhost","root", "12345678", "im_market_db");
$mysqli->query("SET NAMES 'utf8'");
if ($mysqli->connect_errno) {
    echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

//Random logins
function logins() {
    $symbols = "QWERTYUIPASDFGHJKZXCVBNM"."qwertyuipasdfghjkzxcvbnm"."12345689";
    while($i<=6) {
        $word .= $symbols[mt_rand(0, strlen($symbols)-4)];
        $i++;
    }
    return $word;
}

//Random passwords
function passwords() {
    $symbols = "QWERTYUIPASDFGHJKZXCVBNM"."qwertyuipasdfghjkzxcvbnm"."12345689";
    while($i<=10) {
        $word .= $symbols[mt_rand(0, strlen($symbols)-4)];
        $i++;
    }
    return $word;
}

//Random registration date
function reg_rand_date() {
    $start = mktime(0,0,0,1,1,2013);
    $end  = mktime(0,0,0,1,1,2014);
    $randomStamp = rand($start,$end);
    return date('d-m-Y',$randomStamp);
}

//Random last visit date
function last_rand_date() {
    $start = mktime(0,0,0,1,1,2014);
    $end  = mktime(0,0,0,12,31,2014);
    $randomStamp = rand($start,$end);
    return date('d-m-Y',$randomStamp);
}

// Random flag value (on/off)
function flag() {
    return rand(0,1);
}



///////   `User` table creation
$qq = [];
$count = 0;
$ip = rand(0, 255);


//Add 10000 users
for($i = 1; $i <= 10000; $i++) {
    $login = logins();
    $password = passwords();
    $reg_rand_d = reg_rand_date();
    $last_rand_d = last_rand_date();
    $flags = flag();

    //Adding same ip first number for every 10 users
    if ($count > 10) {
        $ip = rand(0, 255);
        $count = 0;
    } else {
        $count++;
    }
    $_ip = $ip;

    //Adding all values to array
    $qq[] = "(NULL, '".$login."', '". $password ."', '". $reg_rand_d ."', '". $last_rand_d ."', '". $_ip ."', '".  $flags ."')";
}


//Insert all values into `Users`
$qa = "INSERT INTO `Users` (`uid`, `login`, `password`, `registration_date`, `last_visit_date`, `ip`, `flag`) VALUES ".implode(',',$qq);
$mysqli->query($qa);

//Insert 1-2-3 random groups into `GROUPLIST` table for each user
$mysqli->query("INSERT INTO `GROUPLIST` (`Users_id`, `Group_id`) VALUES (1,1)");
$mysqli->query("INSERT INTO `GROUPLIST` (`Users_id`, `Group_id`)
  ( SELECT u.uid, ROUND((RAND() * (4-2))+2) as group_id FROM `Users` as u
  WHERE u.uid != 1 ORDER BY RAND() LIMIT 10000 );");

$mysqli->query("INSERT INTO `GROUPLIST` (`Users_id`, `Group_id`)
  ( SELECT u.uid, group_id.id FROM `Users` as u
  LEFT JOIN `GROUPLIST` as g_list ON g_list.Users_id = u.uid
  LEFT JOIN `Group_id` as group_id ON group_id.id != 1 AND group_id.id != g_list.Group_id
WHERE u.uid != 1 AND group_id.id IS NOT NULL ORDER BY RAND() LIMIT 6000 );");


//Insert random sales and partners for each user in `Sales` table
$mysqli->query("INSERT INTO `Sales` (`uid`, `pid`,`iid`)
  ( SELECT Users.uid, ROUND((RAND() * (10-0))+0), ROUND((RAND() * (2-1))+1)  FROM `Users`
  );");


$mysqli->query("INSERT INTO `Sales` (`uid`, `pid`,`iid`)
  ( SELECT Users.uid, COALESCE(Partners.pid, 0), Items.iid FROM `Users`
  LEFT JOIN `Sales` ON Sales.uid = Users.uid
  LEFT JOIN `Partners` ON Partners.pid = Sales.pid
  LEFT JOIN `Items` ON Items.iid != Sales.iid
  ORDER BY RAND() LIMIT 5000
  )");



/////////////////// All sql queries
//1. Numbers of purchases (where user buy items by himself)
//SELECT Sales.uid as "id пользователя", Users.login as "Логин", COUNT(Sales.iid) as "Количество покупок" FROM `Sales` LEFT JOIN `Users` ON Users.uid = Sales.uid
//WHERE Sales.pid = 0 AND Users.flag = 1
//GROUP BY(Sales.uid)


//2. How much money user waste on his items
//SELECT Sales.uid as "id пользователя", Users.login as "Логин", SUM(Items.price) as "Потраченая сумма" FROM `Sales` LEFT JOIN `Users` ON Users.uid = Sales.uid LEFT JOIN `Items` ON Sales.iid = Items.iid
//WHERE Sales.pid = 0
//GROUP BY(Sales.uid)


//3. How much users were sponsored by each partner
//SELECT  Partners.pid as "ID партнера", Partners.partner_name as Партнер,  COUNT(DISTINCT Sales.uid) as "Количество юзеров" FROM Sales LEFT JOIN Partners ON Sales.pid = Partners.pid
//WHERE Sales.pid NOT IN (0)
//GROUP BY Partners.pid
//ORDER BY `Партнер` ASC


//4. How much money each of partners waste on each user
//SELECT Partners.pid as "ID партнера", Partners.partner_name as Партнер, Sales.uid as "ID юзера", SUM(Items.price) as "Сумма денег" FROM Sales LEFT JOIN Partners ON Sales.pid = Partners.pid LEFT JOIN Items ON Sales.iid = Items.iid
//WHERE Sales.pid NOT IN (0)
//GROUP BY Sales.uid, Sales.pid
//ORDER BY `Партнер` ASC


//5. How much money each of partners waste in all
//SELECT Partners.pid as "ID партнера", Partners.partner_name as Партнер,  SUM(Items.price) as "Всего потрачено $" FROM Sales LEFT JOIN Partners ON Sales.pid = Partners.pid LEFT JOIN Items ON Sales.iid = Items.iid
//WHERE Sales.pid NOT IN (0)
//GROUP BY  Sales.pid
//ORDER BY `Партнер` ASC

$mysqli->close();
?>